/*
Copyright 2017 Mario Kleinsasser and Bernhard Rausch

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"bufio"
	"bytes"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"path"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"text/template"
	"time"

	"golang.org/x/net/context"

	// golang profiler
	_ "net/http/pprof"

	// docker client
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"

	// colorize output
	"github.com/gorilla/mux"
	"github.com/logrusorgru/aurora"

	// cron library
	"github.com/robfig/cron"

	// log library
	log "github.com/sirupsen/logrus"

	// fluent style command line parsing
	kingpin "gopkg.in/alecthomas/kingpin.v2"

	// sprig template extensions
	"github.com/Masterminds/sprig"
)

var mainloop bool
var ctrlcmd *exec.Cmd
var dockerclient *client.Client
var configfile string

// Version is the version number used by the make script
var Version string

// Versionname is the codename used by the make script
var Versionname string

// Build is the build hash used by the make script
var Build string

// Buildtime is the timestamp used by the make script
var Buildtime string

// fluent style command line parsing
var (
	app = kingpin.New("bosnd", "The boatswain daemon for loadbalancing")

	versionCommand = app.Command("version", "Print version information")

	runCommand       = app.Command("run", "Runs the bosnd")
	runCommandConfig = app.Flag("config", "The config file to use.").Default("/config/bosnd.yml").Short('c').String()
)

func isprocessrunningps(config *Config) (running bool) {
	// get all folders from proc filesystem
	running = false

	files, _ := ioutil.ReadDir("/proc")
	for _, f := range files {

		// check if folder is a integer (process number)
		if _, err := strconv.Atoi(f.Name()); err == nil {
			// open status file of process
			f, err := os.Open("/proc/" + f.Name() + "/status")
			if err != nil {
				log.Info(err)
				return running
			}

			// read status line by line
			scanner := bufio.NewScanner(f)

			// check if process name in status of process

			for scanner.Scan() {

				re := regexp.MustCompile("^Name:\\s*" + config.Cmd.Processname + ".*")
				match := re.MatchString(scanner.Text())

				if match == true {
					running = true
					log.Debug("Process running: " + strconv.FormatBool(running))
				}

			}
			if running == true {
				return running
			}

		}

	}

	return running

}

var versionTemplate = `The Docker Bosnd - made by https://www.n0r1sk.com
-------------------------------------------------
Version:        {{.Version}}
Version Name:   {{.Versionname}}
Build:          {{.Build}}
Buildtime:      {{.Buildtime}}`

func startprocess(config *Config) {
	log.Info("Start Process!")
	cmd := exec.Command(config.Cmd.Start[0], config.Cmd.Start[1:]...)

	// Attach the process to OS stderr, OS stdout
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout

	// Start the process
	err := cmd.Start()

	if err != nil {
		log.Warn(aurora.Cyan(err.Error()))
	}
	ctrlcmd = cmd

	// just give the process some time to start
	time.Sleep(time.Duration(250) * time.Millisecond)
	ok := isprocessrunningps(config)
	if ok == true {
		log.Info(aurora.Green("Process started"))
	}
}

func reloadprocess(config *Config) {
	log.Info("Reloading Process!")
	cmd := exec.Command(config.Cmd.Reload[0], config.Cmd.Reload[1:]...)
	err := cmd.Start()
	if err != nil {
		log.Warn(aurora.Cyan(err.Error()))
	}
	cmd.Wait()
	isprocessrunningps(config)
}

func writeconfig(config *Config, data interface{}) (changed bool) {

	changed = false

	log.Debug(config.Templates)
	for k, v := range config.Templates {
		log.Debug("Processing Template: " + k)
		log.Debug("Template values: ", v)

		//  open template
		name := path.Base(v.Src)
		t, err := template.New(name).Funcs(sprig.TxtFuncMap()).ParseFiles(v.Src)
		if err != nil {
			log.Error(err)
			continue
		}

		// process template
		var tpl bytes.Buffer
		err = t.Execute(&tpl, data)
		if err != nil {
			log.Error(err)
			continue
		}

		// create md5 of result
		md5tpl := fmt.Sprintf("%x", md5.Sum([]byte(tpl.String())))
		log.Debug("MD5 of CONF " + v.Src + ": " + md5tpl)
		// log.Debug("TPL: " + tpl.String())

		// open existing config, read it to memory
		exconf, errexconf := ioutil.ReadFile(v.Dst)
		if errexconf != nil {
			log.Warn("Cannot read existing conf!")
			log.Warn(errexconf)
		}

		md5exconf := fmt.Sprintf("%x", md5.Sum(exconf))
		log.Debug("MD5 of EXCONF" + v.Src + ": " + md5exconf)
		// log.Debug("TPL: " + string(exconf[:]))

		// comapre md5 and write config if needed
		if md5tpl == md5exconf {
			log.Info(aurora.Green("MD5 sums of " + v.Src + " equal! Nothing to do."))
			continue
		}

		log.Info(aurora.Brown("MD5 sums of  " + v.Src + " different writing new conf!"))

		// overwrite existing conf
		err = ioutil.WriteFile(v.Dst, []byte(tpl.String()), 0644)
		if err != nil {
			log.Error("Cannot write config file.")
			log.Error(err)
		}

		changed = true

	}

	return changed

}

func getservicelabelFromMidshipman(midshipman, servicename string) (map[string]string, error) {

	var err error
	labels := make(map[string]string)

	for i := 0; i < 3; i++ {
		Client := http.Client{
			Timeout: time.Second * 2, // Maximum of 2 secs
		}

		req, httpErr := http.NewRequest(http.MethodGet, midshipman+"/"+servicename, nil)
		if err != nil {
			log.Warn(httpErr)
			err = httpErr
			time.Sleep(time.Duration(5 * time.Second))
			continue
		}

		req.Header.Set("User-Agent", "bosnd")

		res, getErr := Client.Do(req)
		if getErr != nil {
			log.Warn(getErr)
			err = getErr
			time.Sleep(time.Duration(5 * time.Second))
			continue
		}

		if res.StatusCode != 200 {
			log.Warn("No status code 200 received. Status Code was: " + res.Status)
			err = getErr
			time.Sleep(time.Duration(5 * time.Second))
			continue
		}

		body, readErr := ioutil.ReadAll(res.Body)
		if readErr != nil {
			log.Warn(readErr)
			err = readErr
			time.Sleep(time.Duration(5 * time.Second))
			continue
		}

		defer res.Body.Close()

		unmarshalErr := json.Unmarshal([]byte(body), &labels)
		if err != nil {
			log.Warn(unmarshalErr)
			err = unmarshalErr
			time.Sleep(time.Duration(5 * time.Second))
			continue
		}

		if len(labels) == 0 {
			log.Warn("Midshipman couldn't find " + servicename + "!")
			return map[string]string{}, nil
		}

		log.Debugf("%+v", labels)
		return labels, err
	}

	return labels, err

}

func getservicelabel(ctx context.Context, servicename string) (map[string]string, error) {

	f := filters.NewArgs()
	f.Add("name", servicename)

	opts := types.ServiceListOptions{
		Filters: f,
	}
	s, err := dockerclient.ServiceList(ctx, opts)
	if err != nil {
		return nil, err
	}

	// it can happen, that there is a service on the network which does not exist in the swarm
	if len(s) == 0 {
		log.Warn("Docker Swarm engine error: service " + servicename + " not found!")
		return map[string]string{}, nil
	}

	labels := s[0].Spec.Labels

	if len(labels) == 0 {
		log.Warn("Docker Swarm engine warning: service " + servicename + " no labels defined!")
		return map[string]string{}, nil
	}

	log.Debugf("%+v", labels)
	return labels, nil
}

func getorrefreshdockerclient(config *Config) error {

	var err error
	dockerclient, err = client.NewClient("unix:///var/run/docker.sock", "", nil, nil)
	if err != nil {
		log.Warn(err)
	}
	return err

}

func getdatafromfiledb(config *Config) (interface{}, error) {

	d, _ := ioutil.ReadFile(config.Filedb)
	var data interface{}
	err := json.Unmarshal(d, &data)

	if err != nil {
		return nil, err
	}

	return data, nil
}

func getservicesofnet(config *Config) error {

	ctx := context.Background()
	version, err := dockerclient.ServerVersion(ctx)

	if err != nil {
		err := getorrefreshdockerclient(config)
		return err
	}
	log.Debug(version)

	swarmservices := Swarmservices{}

	for _, netwn := range config.Swarm.Networks {

		f := filters.NewArgs()
		f.Add("name", netwn)

		opts := types.NetworkListOptions{
			Filters: f,
		}

		nl, err := dockerclient.NetworkList(ctx, opts)

		if err != nil {
			err := getorrefreshdockerclient(config)
			return err
		}

		if len(nl) == 0 {
			log.Warn("Given network not found: " + netwn)
			continue
		}

		log.Debug(nl)

		var nid string
		var nn string
		if len(nl) > 1 {
			for _, n := range nl {
				if n.Name == netwn {
					nid = n.ID
					nn = n.Name
				}
			}
		} else {
			nid = nl[0].ID
			nn = nl[0].Name
		}

		log.Debug("Matched network: " + nn)

		n, err := dockerclient.NetworkInspect(ctx, nid, types.NetworkInspectOptions{Verbose: true})

		if err != nil {
			return err
		}

		for k, s := range n.Services {
			if k == "" {
				continue
			}

			ms := Service{}
			ms.Name = k

			if config.Midshipman == "" {
				ms.Labels, err = getservicelabel(ctx, ms.Name)
			} else {
				ms.Labels, err = getservicelabelFromMidshipman(config.Midshipman, ms.Name)
			}

			if err != nil {
				return err
			}

			if len(ms.Labels) == 0 {
				continue
			}

			// get the ips and the task slot to slice
			type tmpdata struct {
				ip   string
				slot string
			}

			var tmpdatas []tmpdata

			// get ips to sclice
			for _, t := range s.Tasks {
				log.Debugf("%+v", t.Info)

				// Check if Host IP is set for Endpoint as otherwise it is a Zombie endpoint
				_, ok := t.Info["Host IP"]
				if !ok {
					log.Warn("HostIP not found in Endpoint " + t.Name + " " + t.EndpointIP)
					continue
				}

				log.Debug(t.Name + " " + t.EndpointIP)
				var td tmpdata
				td.ip = t.EndpointIP
				td.slot = strings.Split(t.Name, ".")[1]
				tmpdatas = append(tmpdatas, td)
			}

			sort.Slice(tmpdatas, func(i, j int) bool {
				return tmpdatas[i].slot < tmpdatas[j].slot
			})

			cnt := 1
			for _, d := range tmpdatas {
				me := Endpoint{}
				me.Address = d.ip
				me.Hostname = k + "-" + d.slot
				ms.Endpoints = append(ms.Endpoints, me)
				cnt++
			}

			swarmservices.Services = append(swarmservices.Services, ms)
		}

	}

	// sort the struct to get deterministic sequence, first the services by name
	sort.Slice(swarmservices.Services, func(i, j int) bool {
		return swarmservices.Services[i].Name < swarmservices.Services[j].Name
	})

	// second, inside the services, by endpoint hostnames
	for _, srv := range swarmservices.Services {
		sort.Slice(srv.Endpoints, func(i, j int) bool {
			return srv.Endpoints[i].Hostname > srv.Endpoints[j].Hostname
		})
	}

	config.Swarm.Services = &swarmservices.Services

	return err

	//j, _ := json.Marshal(*config.Swarm.Services)
	//log.Debug(string(j[:]))
}

func (rcontrol *rc) reload(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	if params["id"] == rcontrol.config.Control.Key {
		log.Info("Reload triggered!")
		reloadprocess(rcontrol.config)
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Reloaded!"))
	}
}

func api(config *Config) {
	router := mux.NewRouter()
	r := &rc{config: config}
	router.HandleFunc("/reload/{id}", r.reload).Methods("GET")
	log.Fatal(http.ListenAndServe("0.0.0.0:"+config.Control.Port, router))
}

func printVersion() {
	type v struct {
		Version     string
		Versionname string
		Build       string
		Buildtime   string
	}

	actversion := v{}
	actversion.Build = Build

	if Version == "" {
		actversion.Version = "Manual Build!"
	} else {
		actversion.Version = Version
	}

	if Buildtime == "" {
		actversion.Buildtime = time.Now().String()
	} else {
		actversion.Buildtime = Buildtime
	}

	actversion.Versionname = Versionname

	tmpl, err := template.New("").Parse(versionTemplate)
	if err != nil {
		log.Warn(err)
	}

	var tpl bytes.Buffer
	tmpl.Execute(&tpl, actversion)
	if err != nil {
		log.Warn(err)
	}

	fmt.Print(tpl.String() + "\n")

	os.Exit(0)
}

func runMain() {

	// ignore all signals of child, the kernel will clean them up, no zombies
	signal.Ignore(syscall.SIGCHLD)

	// configure logrus logger
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	customFormatter.ForceColors = true
	log.SetFormatter(customFormatter)
	log.SetOutput(os.Stdout)

	configfile = *runCommandConfig
	config, ok := ReadConfigfile(configfile)
	if !ok {
		log.Warn(aurora.Red("Error during config parsing, yet continuing!"))
	}

	// set check intervall from config
	if config.Checkintervall == 0 {
		config.Checkintervall = 30
	}

	// get debug flag from config
	if config.Debug == true {
		log.SetLevel(log.DebugLevel)
		go func() {
			log.Println(http.ListenAndServe("0.0.0.0:"+config.Debugport, nil))
		}()
	}

	log.Debug("Configfile Keys and Values: ", config)

	// only take the control into accout if it is configured
	if config.Control.Key != "" {
		go api(config)
	}

	// create a cron job
	if config.Cron.Crontab != "" {
		c := cron.New()
		c.AddFunc(config.Cron.Crontab, func() {
			reloadprocess(config)
			log.Info("Sevice reloaded by cronjob")
		})
		c.Start()
		log.Info("Crontab controled reload started!")
	}

	// only take the swarm into accout if it is configured
	if len(config.Swarm.Networks) != 0 {
		// get docker client for swarm
		err := getorrefreshdockerclient(config)
		if err != nil {
			log.Debug(err)
		}
	}

	// log Midshipman or Docker manager
	if config.Midshipman == "" {
		log.Info("Getting Docker Service labels from Docker socket")
	} else {
		log.Info("Getting Docker Service labels from Midshipman")
	}

	// this will loop forever
	mainloop = true
	var changed bool

	for mainloop == true {

		changed = false

		// reread config file
		ok := ReReadConfigfile(configfile, config)
		if !ok {
			log.Warn(aurora.Red("Error during config parsing, yet continuing!"))
			time.Sleep(time.Duration(config.Checkintervall) * time.Second)
			continue
		}

		// only take Docker Swarm into account, if configured
		if len(config.Swarm.Networks) != 0 {
			// get services from Docker network and Docker services
			// work with the local working config inside the loop
			err := getservicesofnet(config)
			if err != nil {
				log.Debug(err)
				log.Warn(aurora.Red("Error during retrieving information: " + err.Error()))
				time.Sleep(time.Duration(config.Checkintervall) * time.Second)
				continue
			}

			// process config
			changed = writeconfig(config, config.Swarm)

		}

		// only take FileDB into account, if configured
		if len(config.Filedb) != 0 {

			data, err := getdatafromfiledb(config)

			if err != nil {
				log.Debug(err)
				log.Warn(aurora.Red("Error during retrieving information from filedb: " + err.Error()))
				time.Sleep(time.Duration(config.Checkintervall) * time.Second)
				continue
			}

			// process config
			changed = writeconfig(config, data)
		}

		if changed == true {
			if isprocessrunningps(config) {
				reloadprocess(config)
			} else {
				startprocess(config)
			}
		} else {
			if !isprocessrunningps(config) {
				startprocess(config)
			}
		}

		time.Sleep(time.Duration(config.Checkintervall) * time.Second)
	}
}

func main() {

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {

	case "version":
		printVersion()
	case "run":
		runMain()
	}

}
