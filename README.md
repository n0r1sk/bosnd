# Bosnd

![BosnD Sticker](https://gitlab.com/n0r1sk/bosnd/raw/master/sticker/bosnd-sticker-4.png)

BosnD, the boatswain daemon - A dynamic configuration file writer & service reloader for dynamically changing container environments. ```Bosnd``` means boatswain daemon. The word itself comes from ```bos'n``` which is a pronounciation used by people with sea legs.

## What it does

```BosnD``` takes a configuration file as argument and based on that configuration file, it uses the given Docker Swarm connection to retrieve information from the Docker Swarm. Therefore the daemon connects to a Docker Swarm manager (leader or promoted node) via the Docker API. The needed information is collected from the ```docker network inspect -v <network>``` and ```docker service inspect <service>``` commands via API. After the information is retrieved, ```BosnD``` processes the configured Golang templates and writes the resulting configuration files to the desired (configured) locations. Afterwards ```BosnD``` will reload the controlled daemon, which is also configured in the ```bosnd.yml``` config file. Alternatively and recommended, the ```midshipman``` 

## Quickstart

1. Clone the repository enable Docker Swarm:
~~~bash
git clone https://gitlab.com/n0r1sk/bosnd
~~~

If you have not already enabled Docker Swarm mode just do it now:
~~~bash
docker swarm init
~~~

2. Build your own Nginx Docker image which is enhanced with BosnD
~~~bash
cd bosnd/examples/bosnd-nginx
docker build . -t mybosnd
~~~

3. Create a Docker overlay network, start your own BosnD-Nginx image and attach it to the created overlay network
~~~bash
docker network create --driver=overlay --attachable test_net
docker run -d --net=test_net -p 80:80 -v /var/run/docker.sock:/var/run/docker.sock --name=mybosnd mybosnd
~~~

If you run `docker logs -f mybosnd` in a second console afterwards, you will notice, that the current configuraiton is not valid, as there are no endpoints at the moment which can be picked up by the BosnD to create a valid Nginx configuration file currently. Furthermore, for demonstration purpose, the BonsD will try to refresh the configuration every three seconds.

4. Create a Docker Stack whose services are using the same Docker overlay network
~~~bash
cd swarm
docker stack deploy -c teststack.yml test
~~~

Please wait some time, until the defined replicas are running. This might take some time. You can check the current status with:
~~~bash
docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE                       PORTS
1sv0kmogkppo        test_app            replicated          0/2                 n0r1skcom/echohttp:latest   
~~~

After the replicas are online, the following output should appear in the log output of the BosnD.
~~~bash
INFO[2018-12-15 11:19:23] MD5 sums of  /es/tpl/nginx.conf.tpl different writing new conf! 
INFO[2018-12-15 11:19:23] Reloading Process! 
~~~

5. Open a browser an check the Nginx load balancing
Open a browser and open the URL `http://localhost`. The following output should appear:
~~~
================================================================================
Hostname:                     test_app-1
--------------------------------------------------------------------------------
Interface lo:                 127.0.0.1/8
Interface lo:                 10.0.0.15/32
Interface eth0:               10.0.0.16/24
Interface eth1:               172.18.0.4/16
--------------------------------------------------------------------------------
Cache-Control                 max-age=0
Accept                        text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
Accept-Language               de,en-US;q=0.9,en;q=0.8,ru;q=0.7
X-Forwarded-For               172.18.0.1
Connection                    close
X-Real-Ip                     172.18.0.1
X-Forwarded-Proto             http
Upgrade-Insecure-Requests     1
User-Agent                    Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/71.0.3578.80 Chrome/71.0.3578.80 Safari/537.36
Accept-Encoding               gzip, deflate, br
X-Forwarded-Server            localhost
~~~

6. Disable sticky sessions by editing the Docker Swarm deployment
If you would like to disable the stickiness of your Docker Stack, just edit the `examples/bosnd-nginx/swarm/teststack.yml` file and remove the `sticky` label. Afterwards redeploy you Docker Stack.

### Lessons learned
This Quickstart should show, how you can build a custom Docker Image including the BosnD to change configuration files dynamically based on Docker Swarm information. Mounting the `/var/run/docker.sock` file is not needed, if you use additional services like the `Midshipman` which will be probably integrated in BosnD in one of the next version.

You are also not limited to Nginx. Instead you could use any binary which could be configured via config files. The BosnD does not restrict you in which binary programm you would like to use, nor does it restrict you in any kind of labels you would like to define to control your configuration files based on the Golang template language.

Furthermore, you can run multiple BosnD on one Docker host by specifying another port mapping during the `docker run` command, eg `-p "192.168.3.1:80:80"`. This enables you to use one Docker Host with multiple IP addresses to bind you services to, for example `https`.

## Features

- Mutliple configuration templates
- Usable with any daemon binary
- Automatically reload the daemon on every change (if supported by the daemon) 
- Reconfigurable during runtime
- Recognises new Docker stack services which are appearing on the Docker network on the fly
- Add new Docker networks to the Bosnd on the fly
- Optional switchable debug mode including Golang pprof interface


## Why don't use xyz...

```BosnD``` is not meant to replace an already existing software. If you are familiar with Traefik for example, please use it. We have created ```BosnD``` because we were in the need to configure software in the Docker environment in a dynamic way. For example, Traefik is already able to read the running services out of the Docker Swarm events. But for us, thats not all we need and also thats not enough. There are a lot of other services which are not Docker Swarm aware. Imagine the following situation: You have a myriad of Apache Tomcat application servers as backends and they are working perfectly in combination with Apache httpd as a load balancer because the Apache httpd has a very pretty module called ```mod_jk```. Now, if you want to modernize this applications with the Docker Swarm environment, you will face some problems. First, you won't drop Apache httpd, because it is the best for the job. Therefore you need a flexible way to change the configuration of the httpd on the fly and reload it. Also, you won't like to map the Docker socket into the loadbalancer container to read the Docker events because that puts the system on a risk. If something terrible happens, someone can have access to the full Docker Swarm API. Bad. It's better to communicate with the swarm via the external API, the API can be protected by RBAC mechanisms like [Golang casbin](https://github.com/casbin/casbin-authz-plugin). And, last but not least, you may have more than one service like the Apache httpd which you would like to empower with dynamic template based configuration.

## And what about confd?

As we wrote ```BosnD``` we soon recognized, that we are writing something like [confd](https://github.com/kelseyhightower/confd). Thats true. But with a different approach. confd will propagate the affected daemon with the new configuration and then reload/restart it. ```BosnD``` in the opposite will be the number one process inside the container. It is responsible for the invoked daemon. If ```BosnD``` dies, the container dies and it will be restarted (if configured in the compose-swarm.yml). Yes, there is always a discussion if there should be only one process inside a container but this is the decision we made (more than one process) for us because we need a general purpose tool. Next, instead of enforcing the possible combination of labels (like Traefik does;no critics here), we decided to let you label the things you like it because we can use the full capability of the Golang Template language. If you need the port, add a label and then use it in the template.

## But it is not real time...

... you are not listening on the Docker events! ;-)

Please define real time. Real time in computing is, when you can guarantee a system response within a specific time frame. By default, ```BosnD``` will pull the actual state of the Docker network every 30 seconds (that's configurable). This is real time.

## Template files

The template files are working with the [Golang template language](https://golang.org/pkg/text/template/).

## Control

The control interface can be used to force a service reload from the outside via http. This is useful, if you have services which are rely on SSL certificates which are renewed by a LetsEncrypt container. If this container renews the certificates you might want to reload the service which is controlled by the ```BosnD``` too.
~~~yaml
control:
  port: 3333
  key: mykey
~~~

If you enable the control block, you can use a browser (or curl) afterwards to trigger the reload: 
```
$ curl http://127.0.0.1:3333/reload/mykey
$ Reloaded!
```

## Cron daemon reload

This can be used to periodically reload the daemon which is controlled by Bosnd, eg. if you are using LetsEncrpyt certificates, there might be the need to reload the controlled daemon ever n-days.

~~~yaml
Cron struct {
	Crontab string // this is the enable Cron config switch
}
~~~

## Midshipman
Configuration of the midship man url is done via the following config

~~~yaml
Midshipman string // this enables the midshipman interface
~~~

# Full current config options (taken from types.go)
~~~yaml
type Config struct {
	Debug          bool
	Debugport      string
	Checkintervall int64
	Cmd            struct {
		Start       []string
		Reload      []string
		Processname string
	}
	Midshipman string // this enables the midshipman interface
	Templates  map[string]Configfilepair
	Swarm      struct {
		Cacertpem     string
		Clientcertpem string
		Clientkeypem  string
		Domainprefix  string
		Domainzone    string
		Managerurl    string
		Usesocket     bool
		Services      *[]Service
		Networks      []string // this is the enable Swarm config switch
	}
	Filedb  string // this is the enable switch for the json based file db
	Control struct {
		Port string
		Key  string // this is the enable Control config switch
	}
	Cron struct {
		Crontab string // this is the enable Cron config switch
	}
}
~~~
